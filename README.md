# Projet REST

## Prérequis:
Il est nécessaire d'avoir npm installé sur votre machine afin d'utiliser le projet (ou à défaut, Node.Js).

## Installation:

Pour installer le projet, placez vous avec un terminal à la racine du projet.
Tapez la commande suivante: `npm install`

Toutes les dépendances à installer sont contenues dans le fichier package.json en cas de problème.

Afin de générer un fichier JSON contenant des articles de l'API MediaStack,
tapez la commande:
`node js/api.js`

Vous pouvez désormais lancer votre JSON-SERVER avec la commande: `json-server articles.json`

Pour afficher le site, ouvrez le fichier __index.html__.
