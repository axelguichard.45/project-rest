$(function () {

    // Initialisation des variables globales
    let articlesList = null; //Liste des articles
    let numberOfItems = 10; //Nombre d'articles par page
    let first = 0; //Index du premier article affiché
    let currentPage = 1; //Numéro de la page actuelle
    let maxPages = null; //Nombre de pages maximales, calculé dynamiquement en fonction du critère de tri
    let currentArticle = null; //Contient l'article sélectionné
    let triDate = "asc"; //Permet le tri par date (récent / ancien)
    let varPays = ""; //Permet le filtre par pays

    $(document).ready(refreshArticlesList); //Affiche les articles dès que la page est prête
    //Association des fonctions liées au GET
    $("#pagePrec").on("click", pagePrecedente);
    $("#pageSuiv").on("click", pageSuivante);
    $("#refreshBtn").on("click", refreshButton);
    $("#selectPays").on("change", triPays);
    $("#triAncient").on("click", triAncient);
    $("#triRecent").on("click", triRecent);

    //Affiche les articles sans critère
    function refreshArticlesList() {
        $("#currenttask").empty();
        let requeteData = "http://localhost:3000/data";

        fetch(requeteData) //Requete GET
            .then(response => {
                if (response.ok) return response.json(); //Transformation de la répons en JSON
                else throw new Error("Problème Promesse: " + response.status);
            })
            .then(function (articles) {
                articlesList = articles;
                maxPages = Math.ceil(articlesList.length / numberOfItems); // Calcul du nombre de page max
                afficherPageActuelle() //Affiche le système de pagination
                $('#articles').empty();
                for (let i = first; i < first + numberOfItems; i++) { //Insère sous forme de liste à puces les articles
                    if (i < articles.length) {
                        $('#articles')
                            .append($('<li>')
                                .append($('<a>')
                                    .text(articles[i].title)
                                ).on("click", articles[i], afficheArticle)
                            );
                    }
                }
            })
            .catch(onerror);
    }

    //Affiche les articles selon le pays
    async function filtrerPays(pays) {
        $("#currenttask").empty();
        let requeteData = "http://localhost:3000/data?country=" + pays; //Sélection selon le pays
        let response = await fetch(requeteData); //Requete GET
        let articles = await response.json(); //Transformation de la répons en JSON

        $('#articles').empty();
        articlesList = articles;
        maxPages = Math.ceil(articlesList.length / numberOfItems); // Calcul du nombre de page max
        afficherPageActuelle() //Affiche le système de pagination
        for (let i = first; i < first + numberOfItems; i++) { //Insère sous forme de liste à puces les articles
            if (i < articles.length) {
                $('#articles')
                    .append($('<li>')
                        .append($('<a>')
                            .text(articles[i].title)
                        ).on("click", articles[i], afficheArticle)
                    );
            }
        }
    }

    //Affiche les articles selon la date de parution
    async function trierDate(order) {
        $("#currenttask").empty();
        let requeteData = "http://localhost:3000/data?_sort=published_at&_order=" + order; //Sélection la date de parution
        let response = await fetch(requeteData); //Requete GET
        let articles = await response.json(); //Transformation de la répons en JSON

        $('#articles').empty();
        articlesList = articles;
        maxPages = Math.ceil(articlesList.length / numberOfItems); // Calcul du nombre de page max
        afficherPageActuelle() //Affiche le système de pagination
        for (let i = first; i < first + numberOfItems; i++) { //Insère sous forme de liste à puces les articles
            if (i < articles.length) {
                $('#articles')
                    .append($('<li>')
                        .append($('<a>')
                            .text(articles[i].title)
                        ).on("click", articles[i], afficheArticle)
                    );
            }
        }
    }

    //Permet de réafficher les articles sans critère
    function refreshButton() {
        first = 0;
        currentPage = 1;
        $("#pagePrec").off('click').on("click", pagePrecedente);
        $("#pageSuiv").off('click').on("click", pageSuivante);
        refreshArticlesList();
    }

    //Permet d'afficher la page suivante
    function pageSuivante(args) {
        if (first + numberOfItems <= articlesList.length) {
            first += numberOfItems;
            currentPage++;
            let arg;
            try {
                arg = args.data.param;
            } catch (error) {
                arg = "";
            }
            console.log(arg);
            switch (arg) { //Affichage selon le critère utilisé
                case "pays":
                    filtrerPays(varPays);
                    break;
                case "date":
                    trierDate(triDate);
                    break;
                default:
                    refreshArticlesList();
            }
        }
    }

    //Affiche le système de pagination
    function afficherPageActuelle() {
        document.getElementById("infoPage").innerHTML = currentPage + "/" + maxPages;
    }

    //Permet d'afficher la page précedente
    function pagePrecedente(args) {
        if (first - numberOfItems >= 0) {
            first -= numberOfItems;
            currentPage--;
            try {
                arg = args.data.param;
            } catch (error) {
                arg = "";
            }
            console.log(arg);
            switch (arg) { //Affichage selon le critère utilisé
                case "pays":
                    filtrerPays(varPays);
                    break;
                case "date":
                    trierDate(triDate);
                    break;
                default:
                    refreshArticlesList();
            }
        }
    }

    //-------------------------------- Affichage de l'article --------------------------------------//
    function afficheArticle(event) {
        $("#currentArticle").empty();
        currentArticle = event;
        createAffiche();
        fillAffiche(event.data);
    }

    function createAffiche() {
        $("#currentArticle")
            .append($("<div><h2 id='titre'></h2><div><img id='image'></div><p id='auteur'></p><p id='source'></p></div>"))
            .append($("<div><p id='descr'></p></div>"))
            .append($("<div class='buttons'><button id='editBtn'><i class='fas fa-edit'></i></button><button id='deleteBtn'><i class='fas fa-trash-alt'></i></button></div>"));
        $("#currentArticle #editBtn").on("click", currentArticle, editArticle);
        $("#currentArticle #deleteBtn").on("click", deleteArticle);
    }

    function fillAffiche(article) {
        document.querySelector("#currentArticle #titre").innerHTML = article.title;
        if (article.image != null) {
            document.querySelector("#currentArticle #image").src = article.image;
            document.querySelector("#currentArticle #image").alt = "Lien vers l'image corrompu";
        } else {
            document.querySelector("#currentArticle #image").src = "https://thumbs.dreamstime.com/b/ic%C3%B4ne-indisponible-d-image-129166551.jpg";
        }
        document.querySelector("#currentArticle #auteur").innerHTML = "Ecrit par " + article.author + ", publié le " + getFormattedDate(new Date(article.published_at));
        document.querySelector("#currentArticle #descr").innerHTML = article.description;
        document.querySelector("#currentArticle #source").innerHTML = "Source: <a href='" + article.url + "'>" + article.source + "</a>";
    }

    //"Constructeur" d'un article
    function Article(title, author, description, url, source, image, published_at) {
        this.title = title;
        this.author = author;
        this.description = description;
        this.url = url;
        this.source = source;
        this.image = image;
        this.published_at = published_at;
    }

    //Association des fonctions liées aux POST, PUT et DELETE
    $("#addBtn").on("click", addArticle);
    $("div.buttons #editBtn").on("click", currentArticle, editArticle);
    $('div.buttons #deleteBtn').on('click', deleteArticle);

    function addArticle() {
        $("#currentArticle").empty();
        createForm("add");
    }

    function editArticle(event) {
        $("#currentArticle").empty();
        createForm("edit");
        fillForm(event.data);
    }

    //-------------------------------- Affichage du formulaire --------------------------------------//
    function createForm(action) {
        let html = "<div class='form'><label>Titre</label><input type='text' id='titre'></label><label>Auteur</label><input type='text' id='auteur'></label><label>Description</label><textarea id='descr'></textarea></label><label>Image</label><input type='text' id='image'></label><label>Site source</label><input type='text' id='site_source'></label><label>URL source</label><input type='text' id='url_source'></label>";
        if (action == "add") {
            html += "<input type='button' id='inputAdd' value=\"Ajouter l'article\"></div>";
            $("#currentArticle").append($(html));
            $("#currentArticle #inputAdd").on("click", saveAddArticle);
        } else {
            html += "<input type='button' id='inputModif' value=\"Modifier l'article\"></div>";
            $("#currentArticle").append($(html));
            $("#currentArticle #inputModif").on("click", saveModifArticle);
        }
    }

    function fillForm(article) {
        document.querySelector("#currentArticle #titre").value = article.data.title;
        document.querySelector("#currentArticle #auteur").value = article.data.author;
        document.querySelector("#currentArticle #descr").value = article.data.description;
        document.querySelector("#currentArticle #url_source").value = article.data.url;
        document.querySelector("#currentArticle #image").value = article.data.image;
        document.querySelector("#currentArticle #site_source").value = article.data.source;
    }


    function saveAddArticle() {
        let art = new Article(
            $("#currentArticle #titre").val(),
            $("#currentArticle #auteur").val(),
            $("#currentArticle #descr").val(),
            $("#currentArticle #url_source").val(),
            $("#currentArticle #site_source").val(),
            $("#currentArticle #image").val(),
            new Date()
        );
        //Insertion d'un article dans le JSON avec Ajax grâce à la méthode POST
        $.ajax({
            url: "http://localhost:3000/data",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(art), //Transformation de l'objet Article en objet JSON
            dataType: 'json',
            success: function () {
                alert('Add Sucess');
            },
            error: function () {
                alert('Add Error');
            }
        });
        //Incrémentation du nombre totale d'articles dans le JSON afin d'avoir une pagination cohérente
        $.ajax({
            url: "http://localhost:3000/pagination",
            type: "GET",
            dataType: "json",
            success: function (pagination) {
                pagination.count += 1;
                $.ajax({
                    url: "http://localhost:3000/pagination",
                    type: 'PUT',
                    contentType: 'application/json',
                    data: JSON.stringify(pagination),
                    dataType: 'json',
                    success: function () {},
                    error: function () {}
                });
            },
            error: function () {}
        });
        refreshArticlesList();
    }

    function saveModifArticle() {
        let art = new Article(
            $("#currentArticle #titre").val(),
            $("#currentArticle #auteur").val(),
            $("#currentArticle #descr").val(),
            $("#currentArticle #url_source").val(),
            $("#currentArticle #site_source").val(),
            $("#currentArticle #image").val(),
            new Date()
        );
        //Modification d'un article dans le JSON avec Ajax grâce à la méthode PUT
        $.ajax({
            url: "http://localhost:3000/data/" + currentArticle.data.id,
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(art),
            dataType: 'json',
            success: function () {
                alert('Save Sucess');
                refreshArticlesList();
            },
            error: function () {
                alert('Save Error');
            }
        });
    }

    function deleteArticle() {
        //Suppression d'un article dans le JSON avec Ajax grâce à la méthode DELETE
        $.ajax({
            url: "http://localhost:3000/data/" + currentArticle.data.id,
            type: 'DELETE',
            success: function () {
                alert('Save Sucess');
                refreshArticlesList();
            },
            error: function () {
                alert('Save Error');
            }
        });
        $("#currentArticle").empty();
    }

    //Permet de formatter une date au format JOUR/MOIS/ANNEE
    function getFormattedDate(date) {
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let year = date.getFullYear();
        return day + "/" + month + "/" + year;
    }

    //Change le système de tri en cours (Pays)
    function triPays() {
        varPays = $("#selectPays").val();
        first = 0;
        currentPage = 1;
        $("#pagePrec").off('click').bind("click", {
            param: 'pays'
        }, pagePrecedente);
        $("#pageSuiv").off('click').bind("click", {
            param: 'pays'
        }, pageSuivante);
        filtrerPays(varPays);
    }
    
    //Change le système de tri en cours (Date)
    function triAncient() {
        triDate = "asc";
        first = 0;
        currentPage = 1;
        $("#pagePrec").off('click').bind("click", {
            param: 'date'
        }, pagePrecedente);
        $("#pageSuiv").off('click').bind("click", {
            param: 'date'
        }, pageSuivante);
        trierDate(triDate);
    }

    //Change le système de tri en cours (Date)
    function triRecent() {
        triDate = "desc";
        first = 0;
        currentPage = 1;
        $("#pagePrec").off('click').bind("click", {
            param: 'date'
        }, pagePrecedente);
        $("#pageSuiv").off('click').bind("click", {
            param: 'date'
        }, pageSuivante);
        trierDate(triDate);
    }
});