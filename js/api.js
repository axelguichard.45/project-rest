const { JSDOM } = require("jsdom");
const { window } = new JSDOM("");
const $ = require("jquery")(window);

$(function () {
    //Permet de créer un fichier JSON à partir de l'API MediaStack, ce qui permet de GET, POST, PUT, DELETE
    function load() {
        $.ajax({
            url: 'http://api.mediastack.com/v1/news?access_key=64cd09f4ad2bfbe6c392a7b3920985af&languages=fr&limit=100',
            type: 'GET',
            dataType: 'json',
            success: function (article) {
                let idNb = 1;
                for (let art of article.data) { // Ajout d'un id à chaque article
                    art["id"] = idNb;
                    idNb++
                }
                article = JSON.stringify(article);
                const fs = require("fs")
                fs.writeFile('articles.json', article, err => {
                    if (err) {
                        console.log('Error writing file', err)
                    } else {
                        console.log('Successfully wrote file')
                    };
                });
            },
            error: function (req, status, err) {
                alert("ERREUR IMPORT");
            }
        });
    };

    load();

});
